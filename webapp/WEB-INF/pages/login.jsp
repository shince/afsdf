<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<shiro:guest>游客</shiro:guest>
<shiro:authenticated>	<%-- 表示已经登陆 --%>
	<shiro:principal></shiro:principal> <%-- 用户名显示 --%>
</shiro:authenticated>
	<form action="logindo" method="get">
		<input name="name"/>
		<input name="psw"/>
		<h3>${msg}</h3>
		<button type="submit">Ok</button>
	</form>
</body>
</html>