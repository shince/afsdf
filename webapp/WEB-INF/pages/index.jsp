<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>

<%-- URI，是uniform resource identifier，统一资源标识符，用来唯一的标识一个资源。
	   而URL是uniform resource locator，统一资源定位器，它是一种具体的URI，即URL可以用来标识一个资源，
	   而且还指明了如何locate这个资源。而URN，uniform resource name，统一资源命名，是通过名字来标识资源，
	   比如mailto:java-net@java.sun.com。也就是说，URI是以一种抽象的，高层次概念定义统一资源标识，
	   而URL和URN则是具体的资源标识的方式。URL和URN都是一种URI。 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>
		<shiro:principal></shiro:principal>
		,已登陆<a href="loginout">loginout</a>
	</h1>
	
	添加
	修改
	删除
	
	<shiro:hasPermission name="/w">
		<h2>w</h2>
	</shiro:hasPermission>
	<shiro:hasPermission name="/w/hello">
		<h2>hello</h2>
	</shiro:hasPermission>
</body>
</html>