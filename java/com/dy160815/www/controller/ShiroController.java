package com.dy160815.www.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShiroController {

	@RequestMapping("unauth")
	public void unauth() {
		
	}
	
	@RequestMapping("index")
	public void index() {
		
	}
	
	@RequestMapping("loginout")
	public String loginout() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:login";
	}
	@RequestMapping("login")
	public void login() {
		
	}

	@RequestMapping("logindo")
	public String logindo(String name, String psw,Model model) {
		
		Subject subject = SecurityUtils.getSubject();
		
		boolean suc = true;
		
		if (!subject.isAuthenticated()) {
			UsernamePasswordToken upt = new UsernamePasswordToken(name, psw);
			try {
				subject.login(upt);
			} catch (UnknownAccountException e) {
				System.out.println("账号不存在");
				suc = false;
			} catch (IncorrectCredentialsException e) {
				System.out.println("密码错误");
				suc = false;
			}
		}
		if (suc){
			return "redirect:index";
		} else {
//			对外系统要提示用户名或密码错误
			model.addAttribute("msg", "用户名或密码错误");
			return "login";
		}
	}
}
