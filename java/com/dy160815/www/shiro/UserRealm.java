package com.dy160815.www.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class UserRealm extends AuthorizingRealm {

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		
		//从数据库查找用户名
		String userName = (String) principals.getPrimaryPrincipal();
		
		//角色集合
		Set<String> roles = new HashSet<String>();
		roles.add(userName);
		//权限集合
		Set<String> premisssions = new HashSet<String>();
		premisssions.add("/index");
		if (userName.equals("admin")){
			premisssions.add("/w");
			premisssions.add("/w/hello");
		}
		
		//授权
		SimpleAuthorizationInfo authorizationInfo =new SimpleAuthorizationInfo();
		authorizationInfo.addRoles(roles);
		authorizationInfo.addStringPermissions(premisssions);
		
		return authorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		String userName= (String) token.getPrincipal();
		
		if(!userName.equals("admin") && !userName.equals("user")){
			throw  new UnknownAccountException();
		}
		
		
		/**
		 * 交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以在此判断或自定义实现
		 */
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(userName,"123",getName());
		return info;
	}

}
